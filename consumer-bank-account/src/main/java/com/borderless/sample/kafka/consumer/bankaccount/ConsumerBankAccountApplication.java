package com.borderless.sample.kafka.consumer.bankaccount;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConsumerBankAccountApplication {

    public static void main(String[] args) {
        SpringApplication.run(ConsumerBankAccountApplication.class, args);
    }

}
