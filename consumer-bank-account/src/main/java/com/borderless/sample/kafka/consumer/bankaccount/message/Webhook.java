package com.borderless.sample.kafka.consumer.bankaccount.message;

import lombok.Data;

@Data
public class Webhook {
    private String id;
    private String type;
    private String status;
}
