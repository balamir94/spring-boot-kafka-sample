package com.borderless.sample.kafka.consumer.bankaccount.listener;

import com.borderless.sample.kafka.consumer.bankaccount.message.Webhook;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class WebhookListener {

    @KafkaListener(topics = "${messaging.kafka.webhook.topic}", containerFactory = "kafkaListenerContainerFactory")
    public void listen(@Payload Webhook webhook) {
        log.info("Webhook message received on BANK-ACCOUNT Application : {}.", webhook);
    }

}
