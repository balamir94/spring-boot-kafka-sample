package com.borderless.sample.kafka.webhook.domain;

import com.borderless.sample.kafka.webhook.api.messsage.Webhook;

public interface WebhookPublisher {
    void publish(Webhook webhook);
}
