package com.borderless.sample.kafka.webhook.infrastructure;

import com.borderless.sample.kafka.webhook.api.messsage.Webhook;
import com.borderless.sample.kafka.webhook.domain.WebhookPublisher;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Component;
import org.springframework.util.concurrent.ListenableFutureCallback;

@Slf4j
@Component
@RequiredArgsConstructor
public class KafkaWebhookPublisher implements WebhookPublisher {

    @Value("${messaging.kafka.webhook.topic}")
    private String topicName;

    private final KafkaTemplate<String, Object> kafkaTemplate;

    @Override
    public void publish(Webhook webhook) {
        kafkaTemplate.send(topicName, webhook)
                .addCallback(new ListenableFutureCallback<SendResult<String, Object>>() {
                    @Override
                    public void onSuccess(SendResult<String, Object> result) {
                        log.info("Message '{}' sent to kafka with offset : {}", webhook, result.getRecordMetadata().offset());
                    }

                    @Override
                    public void onFailure(Throwable ex) {
                        log.error("Unable to send to message : {}. ex : {}", webhook, ex.getMessage());
                    }
                });
    }

}

