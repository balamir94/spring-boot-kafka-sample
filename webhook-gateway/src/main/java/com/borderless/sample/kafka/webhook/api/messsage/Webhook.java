package com.borderless.sample.kafka.webhook.api.messsage;

import lombok.Data;

@Data
public class Webhook {
    private String id;
    private String type;
    private String status;
}
