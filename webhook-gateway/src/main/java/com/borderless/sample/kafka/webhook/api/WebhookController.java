package com.borderless.sample.kafka.webhook.api;

import com.borderless.sample.kafka.webhook.api.messsage.Webhook;
import com.borderless.sample.kafka.webhook.domain.WebhookPublisher;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/webhook")
@RequiredArgsConstructor
public class WebhookController {

    private final WebhookPublisher webhookPublisher;

    @GetMapping
    public void webhook(@ModelAttribute Webhook webhook) {
        log.info("Webhook Received : {}", webhook);
        webhookPublisher.publish(webhook);
    }

}
