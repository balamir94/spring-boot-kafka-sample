package com.borderless.sample.kafka.consumer.card;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConsumerCardApplication {

    public static void main(String[] args) {
        SpringApplication.run(ConsumerCardApplication.class, args);
    }

}
