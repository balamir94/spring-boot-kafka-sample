package com.borderless.sample.kafka.consumer.card.message;

import lombok.Data;

@Data
public class Webhook {
    private String id;
    private String type;
    private String status;
}
